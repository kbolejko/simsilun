 simsilum - SIMplified SILent UNiverse

  Copyright (C) 2017 Krzysztof Bolejko

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program (see the file "copyright" in this repository);
    If not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


### Compiling and testing ###

In order to use the code, you will need a fortran compiler (for example gfortran) and a plotting software (for example gnuplot).

Copy to code (file "simsilun.f") to your working directory and type

   gfortran -o simsilun simsilun.f

(you may wish to add some optimisation and parallelisation flags, such as -O3 and -fopenmp)

Run the code by typing

   ./simsilun

To plot the results, copy the gnuplot script (file "plot.gpl") to the same directory and type

   gnuplot plot.gpl

This will produce the file "density.eps", with the density contrast obtained based on the code, and for comparison it will also plot the density contrast obtained within the Einstein-de Sitter model. You may then wish to compare the obtained results with Figure 3 from arXiv:1708.xxxx 



### Using and modifying the code ###

The code consists of the main program and three subroutines:

- get_parameters:
this subroutine specify the cosmological parameters, units, and finally the type of virialisation scenario, and time step. Please modify this subroutine if you want to change the parameters

- initial_data:
this subroutine specifies the initial data such as initial and final redshift/instant, initial background's density and expansion rate, and initial density contrast. Please modify this subroutine if you want to change the input data.

- silent_evolution
this subroutine evaluates the evolution by solving the evolution equation with the 4th order Runge-Kutta method. If you want to change the method of solving the equation, please modify this subroutine.

Finally the code writes the results to the file "density". It writes the initial density contrast (column 1), final density (column 2), and form comparison density obtained within the approximation of linear perturbations around the Einstein-de Sitter background (column 3).


### Acknowledgements ###

The GNU GPLv2 provides you with flexibility when it comes to using the code. However, when using this code for scientific research, please consider acknowledging the author by citing two papers that describe the code and its applications: arXiv:1707.01800, arXiv:1708.xxxx 


